FactoryBot.define do
  factory :job_application do
    status { Faker::Number.binary(digits: 1) } #{ Faker::Boolean.boolean }
    created_by { Faker::Number.number(digits: 10) }
    job_post_id { nil }
  end
end