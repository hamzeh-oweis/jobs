FactoryBot.define do
  factory :job_post do
    title { Faker::Job.title }
    description { Faker::Lorem.paragraph }
    created_by { Faker::Number.number(digits: 10) }
  end
end