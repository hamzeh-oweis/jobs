FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    email { 'foo@bar.com' }
    password { 'foobar' }
    is_admin { 1 } # { Faker::Number.binary(digits: 1) }
  end
end