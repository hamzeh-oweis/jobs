require 'rails_helper'

# Test suite for User model
RSpec.describe User, type: :model do
  # Association test
  # ensure User model has a 1:m relationship with the JobPosts model
  it { should have_many(:job_posts) }
  # ensure User model has a 1:m relationship with the JobApplications model
  it { should have_many(:job_applications) }
  
  # Validation tests
  # ensure name, email and password_digest are present before save
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password_digest) }
  it { should validate_presence_of(:is_admin) }
end