require 'rails_helper'

# Test suite for the JobPost model
RSpec.describe JobPost, type: :model do
  # Association test
  # ensure JobPost model has a 1:m relationship with the JobApplication model
  it { should have_many(:job_applications).dependent(:destroy) } 
  
  # Validation tests
  # ensure columns title, description and created_by are present before saving
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:created_by) }
end
