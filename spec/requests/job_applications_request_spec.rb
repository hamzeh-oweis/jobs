require 'rails_helper'

RSpec.describe "JobApplications", type: :request do
  # Initialize the test data
  let(:user) { create(:user, is_admin: 0) }
  let!(:job_post) { create(:job_post, created_by: user.id) }
  let(:valid_job_post_id) { job_post.id }
  let(:invalid_job_post_id) { 0 }

  let!(:job_applications) { create_list(:job_application, 20, job_post_id: job_post.id) }
  let(:valid_job_application_id) { job_applications.first.id }
  let(:invalid_job_application_id) { 0 }

  let(:headers) { valid_headers }

  # Test suite for GET /job_posts/:post_id/job_applications
  describe 'GET /job_posts/:job_post_id/job_applications' do

    context 'when job post exists' do
      before { get "/job_posts/#{valid_job_post_id}/job_applications", params: {}, headers: headers }
      
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all job post applications' do
        expect(json.size).to eq(20)
      end
    end

    context 'when job post does not exist' do
      before { get "/job_posts/#{invalid_job_post_id}/job_applications", params: {}, headers: headers }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find JobPost/)
      end
    end
  end

  # Test suite for GET /job_posts/:job_post_id/job_applications/:job_application_id
  describe 'GET /job_posts/:job_post_id/job_applications/:job_application_id' do

    context 'when job post application exists' do
      before { get "/job_posts/#{valid_job_post_id}/job_applications/#{valid_job_application_id}", params: {}, headers: headers }

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the job application' do
        expect(json['id']).to eq(valid_job_application_id)
      end
    end

    context 'when job post application does not exist' do
      before { get "/job_posts/#{valid_job_post_id}/job_applications/#{invalid_job_application_id}", params: {}, headers: headers }
      
      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find JobApplication/)
      end
    end
  end

  # Test suite for POST /job_posts/:job_post_id/job_applications
  describe 'POST /job_posts/:job_post_id/job_applications' do
    let(:valid_attributes) { { status: 0, created_by: '1' }.to_json }
    let(:invalid_attributes) { { status: 0 }.to_json }

    context 'when request attributes are valid' do
      before { post "/job_posts/#{valid_job_post_id}/job_applications", params: valid_attributes, headers: headers }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/job_posts/#{valid_job_post_id}/job_applications", params: {}, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Status can't be blank/)
      end
    end
  end

  # Test suite for PUT /job_posts/:job_post_id/job_applications/:job_application_id
  describe 'PUT /job_posts/:job_post_id/job_applications/:job_application_id' do
    let(:valid_attributes) { { status: 1 }.to_json }
    
    context 'when job application exists' do
      before { put "/job_posts/#{valid_job_post_id}/job_applications/#{valid_job_application_id}", params: valid_attributes, headers: headers }

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the job application' do
        updated_job_application = JobApplication.find(valid_job_application_id)
        expect(updated_job_application.status).to eq(1)
      end
    end

    context 'when the job application does not exist' do
      before { put "/job_posts/#{valid_job_post_id}/job_applications/#{invalid_job_application_id}", params: valid_attributes, headers: headers }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find JobApplication/)
      end
    end
  end

  # Test suite for DELETE /job_posts/:job_post_id/job_applications/:job_application_id
  describe 'DELETE /job_posts/:job_post_id/job_applications/:job_application_id' do
    before { delete "/job_posts/#{valid_job_post_id}/job_applications/#{valid_job_application_id}", params: {}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
