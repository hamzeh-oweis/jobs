require 'rails_helper'

RSpec.describe JobApplicationPolicy, type: :policy do
  let(:admin_user) { create(:user, is_admin: 1) }
  let(:user) { create(:user, is_admin: 0) }
  let!(:job_post) { create(:job_post, created_by: admin_user.id) }
  let(:job_application) { create(:job_application, job_post_id: job_post.id) }

  subject { described_class }

  # permissions ".scope" do
  #   it "Job posts requested" do
  #     expect(subject).to permit(nil, job_application)
  #   end
  # end

  permissions :index? do
    it "Job applications requested" do
      expect(subject).to permit(nil, job_application)
    end
  end

  permissions :show? do
    it "Job applications requested" do
      expect(subject).to permit(nil, job_application)
    end
  end

  permissions :create? do
    it "Job Seeker" do
      expect(subject).to permit(user, job_application)
    end
    it "Admin User" do
      expect(subject).not_to permit(admin_user, job_application)
    end
  end

  permissions :update? do
    it "Job Seeker" do
      expect(subject).to permit(user, job_application)
    end
    it "Admin User" do
      expect(subject).not_to permit(admin_user, job_application)
    end
  end

  permissions :destroy? do
    it "Job Seeker" do
      expect(subject).to permit(user, job_application)
    end
    it "Admin User" do
      expect(subject).not_to permit(admin_user, job_application)
    end
  end
end
