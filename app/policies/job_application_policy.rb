class JobApplicationPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def create?
    @user.is_admin == 0
  end

  def update?
    @user.is_admin == 0
  end

  def destroy?
    @user.is_admin == 0
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
