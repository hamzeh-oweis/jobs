class CreateJobPosts < ActiveRecord::Migration[6.1]
  def change
    create_table :job_posts do |t|
      t.string :title
      t.string :description
      t.string :created_by

      t.timestamps
    end
  end
end
